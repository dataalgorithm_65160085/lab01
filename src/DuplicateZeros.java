import java.util.Scanner;

public class DuplicateZeros {
    public static int arr[];

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int arr[] = { 1, 0, 2, 3, 0, 4, 5, 0 };
        printArrOrigin(arr);
        DuplicateZeros(arr);
        int arr2[] = { 1, 2, 3 };
        printArrOrigin(arr2);
        DuplicateZeros(arr2);
    }

    private static void printArrOrigin(int[] arr) {
        int arrSize = arr.length;
        System.out.print("Input : ");
        for (int i = 0; i < arrSize; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    public static void DuplicateZeros(int[] arr) {
        int arrSize = arr.length;
        int arrCopy[] = new int[arrSize * 2];
        int j = 0;

        for (int i = 0; i < arrSize; i++) {
            arrCopy[j++] = arr[i];
            if (arr[i] == 0) {
                arrCopy[j++] = 0;
            }
        }

        System.out.print("Output: ");
        for (int i = 0; i < arrSize; i++) {
            System.out.print(arrCopy[i] + " ");
        }
        System.out.println();
    }

}
